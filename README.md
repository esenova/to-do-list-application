# To Do List Application 

## Description 

* Create a console-baseed-to-do list application. Users should be able to add tasks, mark them as complete, view the list, and remove tasks.

## Requirements 

1. Add Task: Users can add tasks to the to-do list
2. View Tasks: Users can view all the tasks in the to-do list.
3. Mark as Complete: Users can mark tasks as complete.
4. Remove Task: Users can remove tasks from the list.

##Implementation: 

* You can implement this project using basic Python features and data structures. Here's a simple structure to get you started:

class TodoList:
    def __init__(self):
        self.tasks = []
    
    def add_task(self, task):
        self.tasks.append({"task": task, "completed": False})

    def view_tasks(self):
        for i, task in enumerate(self.tasks, start=1):
            status = "Done" if task["completed"] else "Not Done"
            print(f"{i}. {task['task']} - {status}")

    def mark_complete(self, task_index):
        if 0 < task_index <= len(self.tasks):
            self.tasks[task_index - 1]["completed"] = True
            print("Task marked as complete.")
        else:
            print("Invalid task index.")

    def remove_task(self, task_index):
        if 0 < task_index <= len(self.tasks):
            del self.tasks[task_index - 1]
            print("Task removed.")
        else:
            print("Invalid task index.")

def main():
    todo_list = TodoList()
    
    while True:
        print("\n==== To-Do List ====")
        print("1. Add Task")
        print("2. View Tasks")
        print("3. Mark as Complete")
        print("4. Remove Task")
        print("5. Quit")

        choice = input("Enter your choice: ")

        if choice == "1":
            task = input("Enter the task: ")
            todo_list.add_task(task)
        elif choice == "2":
            todo_list.view_tasks()
        elif choice == "3":
            task_index = int(input("Enter the task index to mark as complete: "))
            todo_list.mark_complete(task_index)
        elif choice == "4":
            task_index = int(input("Enter the task index to remove: "))
            todo_list.remove_task(task_index)
        elif choice == "5":
            print("Goodbye!")
            break
        else:
            print("Invalid choice. Please enter a valid option.")
if __name__ == "__main__":
    main()

This project involves basic user input, data structures, and control flow. Once you're comfortable with this, you can consider adding more features or even creating a graphical user interface (GUI) for a more advanced version.
